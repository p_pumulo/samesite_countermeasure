#!/bin/bash

npm init -y
npm install express

# Kotlinファイルのコンパイル
kotlinc MyFile.kt -include-runtime -d MyFile.jar

# コンパイルされたJarファイルを実行
java -jar MyFile.jar
